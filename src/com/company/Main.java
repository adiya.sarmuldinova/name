package com.company;
import java.sql.Connection;
import java.sql.ResultSet;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class 1Main {

    public static Scanner in = new Scanner(System.in);

    public static void main(String[] args) throws SQLException {
        DB.setConnection();
        DB.createTable("CREATE TABLE IF NOT EXISTS phone(" +
                "number INT NOT NULL, " +
                "name VARCHAR(64)" +
                ")");

        System.out.println("Телефонная книжка v1.0 created by Adiyalllo");
        commands();
    }

    public static void commands() throws SQLException {
        System.out.println("Введите команду: ");
        System.out.println("1. Показать весь список.");
        System.out.println("2. Добавить контакт");
        System.out.println("3. Удалить номер");
        System.out.println("0. exit");
        int cmd = in.nextInt();

        switch (cmd){
            case 1:
                show();
                break;
            case 2:
                add();
                break;
            case 3:
                delete();
                break;
            case 0:
                System.exit(0);
            default:
                System.out.println("Не парвильная команда! ВВедите команду обратно!");
                commands();
        }
    }

    public static void add() throws SQLException {
        String name;
        int number;

        System.out.println("Напишие имя: ");
        name = in.next();
        System.out.println("Напишите номер: ");
        number = in.nextInt();

        DB.insert("INSERT INTO phone VALUES('" + number + "', '" + name + "')");

        System.out.println("Номер успешенео добавлен!");

        commands();
    }

    public static void show() throws SQLException {
        ResultSet phones = DB.select_query("SELECT * FROM phone");

//        Showing ResultSet with while loop
        while (phones.next()) {
            String number = phones.getString(1);
            String name = phones.getString(2);

            System.out.println(name + " - " + number);
        }

        commands();
    }

    public static void delete() throws SQLException {
        System.out.println("Напишите номер которую хотите удалить: ");
        int number = in.nextInt();
        DB.deleteRow("DELETE FROM phone WHERE number = " + number);
        System.out.println("Номер успешно удален.");

        commands();
    }
}
