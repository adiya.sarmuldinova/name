package com.company;

import java.sql.*;

public class DB {
    //    SQL elements
    public static String URL = "jdbc:postgresql://localhost:5432/Adiyalllo";
    public static String username = "postgres";
    public static String password = "123";


    public static Connection connection = null;
    public static Statement statement = null;

    public static void setConnection() {
        try {
//            Register JDBC Drivers
            Class.forName("org.postgresql.Driver");

//            Open connection
            connection = DriverManager.getConnection(URL, username, password);
            if (connection != null) {
                System.out.println("Connect true.");
            } else {
                System.out.println("Connect false.");
            }

//            create statement

        } catch (Exception e) {
            System.out.println(e);
        }

    }


    //    method for creating table
    public static void createTable(String create_query ) throws SQLException {
        statement = connection.createStatement();
        statement.executeUpdate(create_query);
        System.out.println("Table created.");
    }

    //    drop table method
    public static void dropTable(String delete_table) throws SQLException {
        statement = connection.createStatement();
        statement.executeUpdate("DROP TABLE " + delete_table);
        System.out.println("Table deleted.");
    }

    //    insert method
    public static void insert(String insert_query) throws SQLException {
        statement = connection.createStatement();
        statement.executeUpdate(insert_query);
        System.out.println("Data inserted.");
    }

    //    select method
    public static ResultSet select_query(String select_query) throws SQLException {
        return statement.executeQuery(select_query);
    }

    public static void deleteRow(String create_query ) throws SQLException {
        statement = connection.createStatement();
        statement.executeUpdate(create_query);

    }

//    show select
//    public static void select(String select_query) throws SQLException {
//        ResultSet select_result = DB.select_query(select_query);
//        while (select_result.next()){
//            String name = select_result.getString(1);
//            String surname = select_result.getString(2);
//            System.out.print("Student ID(" + student_id + ") - Name: " + name + ", Surname: " + surname + ".");
//        }
//    }
}
